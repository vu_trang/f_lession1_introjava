package lession1.introjava;

public class PTB2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long a = 3;
		long b = 4;
		long c = 1;
		long delta = 0;
		double x1 = 0;
		double x2 = 0;
		
		System.out.println("Giai phuong trinh bac 2 (a#0):");
		delta = b*b - 4*a*c;
		
		if (delta<0) System.out.println("Phuong trinh vo nghiem");
		else if (delta == 0) {
			 x1 = x2 = -b/(2*a);
			 System.out.println("Phuong trinh co nghiem kep x1 = x2 =  " + x1  );
		} else if (delta >0) {
			x1 = (-b + Math.sqrt(delta))/2/a;
			x2 = (-b - Math.sqrt(delta))/2/a;
			System.out.println("Phuong trinh co 2 nghiem la x1 = " + x1 + " va x2 = " + x2);
		}

	}

}
